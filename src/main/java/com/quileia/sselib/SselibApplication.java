package com.quileia.sselib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SselibApplication {

	public static void main(String[] args) {
		SpringApplication.run(SselibApplication.class, args);
	}

}

package com.quileia.sselib.repository.impl;

import com.quileia.sselib.repository.EmitterRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class EmitterRepositoryImpl implements EmitterRepository {

    public final Map<String, SseEmitter> cutEmitterMap = new ConcurrentHashMap<>();

    public final Map<String, SseEmitter> logisticEmitterMap = new ConcurrentHashMap<>();


    public final Map<String, SseEmitter> fabricRunner = new ConcurrentHashMap<>();

    @Override
    public void addOrReplaceEmitter(String id, SseEmitter emitter, String type) {

        switch (type){
            case "cut":
                cutEmitterMap.put(id, emitter);
                break;
            case "logistic":
                logisticEmitterMap.put(id,emitter);
                break;
            case "fabricRunner":
                fabricRunner.put(id,emitter);
                break;
        }

    }

    @Override
    public SseEmitter remove(String id,String type) {
       SseEmitter emitter = null;
        switch (type){
            case "cut":
                if (cutEmitterMap.containsKey(id)) {
                    emitter = cutEmitterMap.get(id);
                    cutEmitterMap.remove(id);
                }
                break;
            case "logistic":
                if (logisticEmitterMap.containsKey(id)) {
                    emitter = logisticEmitterMap.get(id);
                    logisticEmitterMap.remove(id);
                }
                break;
            case "fabricRunner":
                if (fabricRunner.containsKey(id)) {
                    emitter = fabricRunner.get(id);
                    fabricRunner.remove(id);
                }
                break;
        }
        return emitter;
    }

    @Override
    public Optional<SseEmitter> get(String id,String type) {

        Optional<SseEmitter> emitter = null;
     switch (type){
            case "cut":
                emitter = Optional.ofNullable(cutEmitterMap.get(id));
                break;
            case "logistic":
                emitter = Optional.ofNullable(logisticEmitterMap.get(id));
                break;
            case "fabricRunner":
                emitter = Optional.ofNullable(fabricRunner.get(id));
                 break;
        }
        return emitter;
    }

    @Override
    public void sendAll(String map,String type) {

        switch (type){
            case "cut":
                cutEmitterMap.forEach((s, sseEmitter) -> {
                    try {
                        sseEmitter.send(SseEmitter.event().name("message").data(map));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                break;
            case "logistic":
                logisticEmitterMap.forEach((s, sseEmitter) -> {
                    try {
                        sseEmitter.send(SseEmitter.event().name("message").data(map));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                break;
            case "fabricRunner":
                fabricRunner.forEach((s, sseEmitter) -> {
                    try {
                        sseEmitter.send(SseEmitter.event().name("message").data(map));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                break;
        }
    }
}

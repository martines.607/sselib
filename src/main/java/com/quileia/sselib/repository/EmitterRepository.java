package com.quileia.sselib.repository;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.Map;
import java.util.Optional;

public interface EmitterRepository {

    void addOrReplaceEmitter(String id, SseEmitter emitter, String type);

    SseEmitter remove(String id,String type);

    Optional<SseEmitter> get(String id,String type);

    void sendAll(String map,String type);
}

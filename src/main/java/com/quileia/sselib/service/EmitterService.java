package com.quileia.sselib.service;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Map;

public interface EmitterService {

    SseEmitter createEmitter(String id,String type) throws IOException;

    void closeEmitter(String id,String type);

    void sendAllSubscribe(String map, String type) throws IOException;

}

package com.quileia.sselib.service.impl;

import com.quileia.sselib.repository.EmitterRepository;
import com.quileia.sselib.service.EmitterService;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class EmitterServiceImpl implements EmitterService {

    private final EmitterRepository repository;

    public EmitterServiceImpl(EmitterRepository repository) {
        this.repository = repository;
    }


    @Override
    public SseEmitter createEmitter(String memberId , String type) throws IOException {
        var emitter = new SseEmitter(-1L);
        repository.addOrReplaceEmitter(memberId, emitter,type);
        emitter.onCompletion(() ->repository.remove(memberId,type));
        return emitter;
    }

    @Override
    public void closeEmitter(String id,String type) {
        var emitter = repository.get(id,type);
        emitter.ifPresent(e -> {
            emitter.get().complete();
            repository.remove(id,type);
        });
    }

    @Override
    public void sendAllSubscribe(String map,String type) throws IOException {
            repository.sendAll(map,type);
        }
    }

